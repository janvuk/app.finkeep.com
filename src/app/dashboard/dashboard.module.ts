import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/auth-guard.service';


const routes: Routes = [
    {
        path: 'transactions',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
            loginStatusShouldBe: true,
        },
    },
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [],
})
export class DashboardModule {
}
