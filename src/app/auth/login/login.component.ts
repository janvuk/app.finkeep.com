import { Component, ViewChild } from '@angular/core';
import { SmartInputValue } from '../../shared/form/smart-input/component';
import { ApiService } from '../../shared/api.service';
import { SmartCompleteData } from '../../shared/form/smart-complete/component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ValidationService } from '../../shared/form/validation.service';

@Component({
    templateUrl: './login.component.html',
})
export class LoginComponent {
    form: FormGroup;
    accounts: SmartCompleteData[] = [];
    categories: SmartCompleteData[] = [];
    values: SmartInputValue[] = [
        {
            title: 'Hi there',
            subtitle: '',
            inverted: false,
        },
    ];
    @ViewChild('elPresetName') elPresetName;
    @ViewChild('elDatesDue') elDatesDue;

    constructor(private fb: FormBuilder,
                private apiService: ApiService,
                private vs: ValidationService) {
        this.accounts = apiService.getAccounts();
        this.categories = apiService.getCategories();
        this.form = fb.group({
            presetName: [''],
            datesDue: [''],
            datesCompleted: [''],
            accounts: ['', [this.vs.minPills(2)]],
            categories: [''],
            amounts: [''],
            description: [''],
        });
        this.form.controls.datesDue.setValue(JSON.stringify(this.values));
    }
}
