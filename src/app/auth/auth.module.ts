import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { FormModule } from '../shared/form/form.module';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    {
        path: 'auth',
        children: [
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'register',
                component: RegisterComponent,
            },
        ],
    },
];

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormModule,
    ],
    exports: [
        RouterModule,
    ],
    providers: [],
})
export class AuthModule {
}
