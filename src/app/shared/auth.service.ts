import { EventEmitter, Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ng2-cookies';
import { ApiService } from './api.service';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
    user: any = false;
    loginStatusChange = new EventEmitter<any>();

    constructor(private http: HttpClient,
                private cookieService: CookieService,
                private apiService: ApiService) {
    }

    /**
     * Set/Delete user session
     * @param sessionHash
     */
    userSessionSet(sessionHash) {
        if (sessionHash !== false) {
            this.cookieService.set(environment.userHashCookieName, sessionHash, 30, '/');
        } else {
            this.cookieService.delete(environment.userHashCookieName, '/');
        }
    }

    /**
     * Return user session
     * @returns {string}
     */
    userSessionGet() {
        return this.cookieService.get(environment.userHashCookieName);
    }

    /**
     * Set user
     * @param user
     */
    userSet(user) {
        this.user = user;
        this.loginStatusChange.emit(this.user);
    }

    /**
     * If cookie is set check validity with API and fetch user object
     * @returns {Promise<any>}
     */
    userGet() {
        return new Promise((resolve, reject) => {
            if (this.userSessionGet() !== '') {
                this.apiService.request({
                    method: 'get',
                    url: '/users/',
                    sessionHash: true,
                }).subscribe((response) => {
                    this.userSet(response);
                    resolve();
                }, (error) => {
                    this.userSet(false);
                    this.userSessionSet(false);
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    login(email, password) {
        return this.apiService.request({
            method: 'post',
            url: '/users/login/',
            params: {
                email: email,
                password: password,
            },
            applicationHashPublic: true,
        }).map((res) => {
            this.userSessionSet(res['session_hash']);
            this.userSet(res);
        });
    }

    register(data) {
        return this.apiService.request({
            method: 'post',
            url: '/users/client/',
            params: data,
            applicationHashPublic: true,
        });
    }

    forgot(email, reCaptcha) {
        return this.apiService.request({
            method: 'put',
            url: '/users/forgot/',
            params: {
                email: email,
                re_captcha: reCaptcha,
            },
            applicationHashPublic: true,
        });
    }

    reset(emailForgotHash, password) {
        return this.apiService.request({
            method: 'put',
            url: '/users/reset/',
            params: {
                email_forgot_hash: emailForgotHash,
                password: password,
            },
        });
    }

    emailAvailable(email) {
        return this.apiService.request({
            method: 'get',
            url: '/users/email/',
            params: {
                email: email,
            },
            applicationHashPublic: true,
        }).map(res => res === 0);
    }

    /**
     * Logout user
     */
    logout() {
        this.userSessionSet(false);
        this.userSet(false);
    }

    /**
     * Returns whether the user is logged in
     */
    isLoggedIn() {
        return !!this.user;
    }
}
