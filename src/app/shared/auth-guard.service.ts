import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot) {
        const loginStatusShouldBe = typeof route.data.loginStatusShouldBe === 'undefined' ? null : !!route.data.loginStatusShouldBe;
        const isLoggedIn = this.authService.isLoggedIn();
        // Requires to be logged in
        if (loginStatusShouldBe && !isLoggedIn) {
            this.router.navigate(['/auth/login']);
            return false;
        }
        // Cannot visit page is logged in
        if (!loginStatusShouldBe && isLoggedIn) {
            this.router.navigate(['/']);
            return false;
        }
        return true;
    }
}
