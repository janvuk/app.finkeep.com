import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-form-smart-wrap',
    template: `
        <div
            [ngStyle]="{'min-width': minWidth}"
            [ngClass]="{'toRight': toRight}">
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ['./component.css'],
})
export class SmartWrapComponent {
    @Input() minWidth = null;
    @Input() toRight = false;
}
