import { Component, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core';
import { SmartInputTheme, SmartInputType } from '../smart-input/component';
import { SmartCompleteData } from '../smart-complete/component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../api.service';
import { ValidationService } from '../validation.service';
import { FormService } from '../form.service';

@Component({
    selector: 'app-form-smart-amount-range',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})

export class SmartAmountRangeComponent {
    opened = false;
    shiftDown = false;
    form: FormGroup;
    currencies: SmartCompleteData[] = [];
    types: SmartCompleteData[] = [];
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iValue = '';
    @Input() iMultiple = true;
    @Input() iCanAdd = false;
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iData: SmartCompleteData[] = [];
    @Input() bValueText = '';
    @Output() bValueTextChange = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @ViewChild('smartInput') smartInput;

    @HostListener('window:keydown', ['$event']) windowKeyDown($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        }
    }

    @HostListener('window:keyup', ['$event']) windowKeyUp($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = false;
        }
    }

    constructor(private fb: FormBuilder,
                private vs: ValidationService,
                private apiService: ApiService,
                private formService: FormService) {
        this.currencies = apiService.getCurrencies();
        this.types = apiService.getTransactionTypes();
        this.form = fb.group({
            type: [''],
            currency: ['', vs.minPills(1)],
            from: ['', Validators.min(1)],
            to: ['', Validators.min(0)],
        }, {validator: this.vs.amountRangeGroup.bind(this.vs)});
    }

    submit() {
        if (this.form.valid) {
            const typeValue = this.vs.parseJson(this.form.controls.type.value);
            const currencyValue = this.vs.parseJson(this.form.controls.currency.value);
            const fromValue = this.form.controls.from.value;
            const toValue = this.form.controls.to.value;
            // Join pill title
            let titleValue = '';
            if (typeValue && typeValue.length === 1) {
                titleValue += typeValue[0].title + ' ';
                if (currencyValue && currencyValue.length === 1) {
                    titleValue += 'in ';
                }
            }
            if (currencyValue && currencyValue.length === 1) {
                titleValue += currencyValue[0].title + ' ';
            }
            if (fromValue && fromValue.length > 0) {
                titleValue += `from ${fromValue} `;
            }
            if (toValue && toValue.length > 0) {
                titleValue += `to ${toValue} `;
            }
            const formControl = this.iFormGroup.controls[this.iFormControlName];
            const values = this.formService.getValuesFromValue(formControl.value).concat([{
                title: titleValue,
                inverted: false,
                children: {
                    type: typeValue,
                    currency: currencyValue,
                    from: fromValue,
                    to: toValue,
                },
            }]);
            formControl.setValue(JSON.stringify(values));
            this.form.reset();
            this.smartInput.inputFocus();
        }
    }

    tabCloses($event, shiftDown) {
        if ($event.keyCode === 9 && (!shiftDown && !this.shiftDown) || (shiftDown && this.shiftDown)) {
            this.opened = false;
        }
    }
}
