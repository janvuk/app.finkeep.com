import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { SmartInputTheme, SmartInputType } from '../smart-input/component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SmartCompleteData } from '../smart-complete/component';
import { FormService } from '../form.service';
import * as moment from 'moment';
import { DateService } from '../date.service';

@Component({
    selector: 'app-form-smart-date-range-picker',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})

export class SmartDateRangePickerComponent implements OnInit {
    form: FormGroup;
    opened = false;
    shiftDown = false;
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iValue = '';
    @Input() iMultiple = true;
    @Input() iCanAdd = false;
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iData: SmartCompleteData[] = [];
    @Input() dateFrom;
    @Input() dateTo;
    @Input() bValueText = '';
    @Output() bValueTextChange = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @ViewChild('smartInput') smartInput;

    @HostListener('window:keydown', ['$event']) windowKeyDown($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        }
    }

    @HostListener('window:keyup', ['$event']) windowKeyUp($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = false;
        }
    }

    constructor(private fb: FormBuilder,
                private formService: FormService,
                private dateService: DateService) {
        this.form = fb.group({
            from: [''],
            to: [''],
        });
    }

    ngOnInit() {
        if (!this.dateFrom) {
            this.form.controls.from.setValue(moment().subtract(1, 'day').format(this.dateService.format));
        }
        if (!this.dateTo) {
            this.form.controls.to.setValue(moment().format(this.dateService.format));
        }
    }

    tabCloses($event, shiftDown) {
        if ($event.keyCode === 9 && (!shiftDown && !this.shiftDown) || (shiftDown && this.shiftDown)) {
            this.opened = false;
        }
    }

    submit() {
        if (this.form.valid) {
            const valueFrom = this.form.controls.from.value;
            const valueTo = this.form.controls.to.value;
            // Build title
            let title = '';
            if (valueFrom.length > 0) {
                title += `${valueFrom} `;
            }
            if (valueFrom.length > 0 && valueFrom.length > 0) {
                title += `${valueFrom} `;
            }
            if (valueTo.length > 0) {
                title += `${valueTo} `;
            }
            title = title.substr(0, title.length - 1);
            const formControl = this.iFormGroup.controls[this.iFormControlName];
            formControl.setValue(JSON.stringify(this.formService.getValuesFromValue(formControl.value).concat([{
                title: title,
                inverted: false,
                meta: {
                    from: valueFrom,
                    to: valueTo,
                },
            }])));
            this.form.reset();
            this.smartInput.inputFocus();
        }
    }
}
