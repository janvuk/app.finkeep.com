import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

export declare type SmartInputType = 'text' | 'password' | 'number';
export declare type SmartInputTheme = 'white' | 'black';

@Component({
    selector: 'app-form-simple-input',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})
export class SimpleInputComponent implements OnInit {
    formControl: AbstractControl;
    shiftDown = false;
    doNotMoveCaret = false;
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iValueText = '';
    @Output() oValueChanged = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @Output() oKeyUp = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oClickedOutside = new EventEmitter();
    @ViewChild('inputText') inputText;
    @ViewChild('inputHidden') inputHidden;

    @HostListener('document:click', ['$event']) clickedOutside($event) {
        if (!this.eRef.nativeElement.contains($event.target)) {
            this.oClickedOutside.emit();
        }
    }

    @HostListener('window:keydown', ['$event']) windowKeyDown($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        }
    }

    @HostListener('window:keyup', ['$event']) windowKeyUp($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = false;
        }
    }

    constructor(private eRef: ElementRef) {
    }

    ngOnInit() {
        this.formControl = this.iFormGroup.controls[this.iFormControlName];
        this.formValueChange();
        // On value change
        this.formControl.valueChanges.subscribe(() => {
            this.formValueChange();
        });
    }

    formValueChange() {
        this.iValueText = this.formControl.value === null ? '' : this.formControl.value;
    }

    inputFocus() {
        this.inputText.nativeElement.focus();
        this.oFocus.emit();
        if (this.doNotMoveCaret === false) {
            this.caretToEnd();
        } else {
            this.doNotMoveCaret = false;
        }
    }

    caretToEnd() {
        const range = document.createRange();
        range.selectNodeContents(this.inputText.nativeElement);
        range.collapse(false);
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);

    }

    keyDown($event) {
        this.oKeyDown.emit($event);
    }

    clearAll() {
        this.formControl.setValue('');
    }

    inputChange() {
        this.formControl.setValue(this.inputText.nativeElement.innerText);
    }
}
