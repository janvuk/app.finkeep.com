import { Injectable } from '@angular/core';
import { SmartInputValue } from './smart-input/component';

@Injectable()
export class FormService {
    getValuesFromValue(value): SmartInputValue[] {
        try {
            if (this.isNotEmpty(value)) {
                return JSON.parse(value);
            }
        } catch (e) {
            console.error(e);
        }
        return [];
    }

    isNotEmpty(value) {
        return value !== '' && value !== null;
    }
}
