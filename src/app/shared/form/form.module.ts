import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SmartInputComponent } from './smart-input/component';
import { SmartCompleteComponent } from './smart-complete/component';
import { SmartSelectComponent } from './smart-select.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { SmartAmountRangeComponent } from './smart-amount-range/component';
import { SmartWrapComponent } from './smart-wrap/component';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidationService } from './validation.service';
import { SimpleInputComponent } from './simple-input/component';
import { ButtonComponent } from './button/component';
import { FormService } from './form.service';
import { SmartDatePickerComponent } from './smart-date-picker/component';
import { SmartCalendarComponent } from './smart-calendar/component';
import { SmartDateRangePickerComponent } from './smart-date-range-picker/component';
import { DateService } from './date.service';

const sharedComponents = [
    SimpleInputComponent,
    SmartInputComponent,
    SmartWrapComponent,
    SmartCompleteComponent,
    SmartSelectComponent,
    SmartAmountRangeComponent,
    ButtonComponent,
    SmartCalendarComponent,
    SmartDatePickerComponent,
    SmartDateRangePickerComponent,
];

@NgModule({
    declarations: sharedComponents,
    imports: [
        CommonModule,
        InlineSVGModule,
        ReactiveFormsModule,
    ],
    exports: sharedComponents,
    providers: [
        FormService,
        ValidationService,
        DateService,
    ],
})
export class FormModule {
}
