import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-form-button',
    template: `
        <button class="{{iColor}} {{iSize}}"
                [type]="iType"
                (click)="oClicked.emit()"
                (focus)="oFocus.emit()"
                (blur)="oBlur.emit()"
                (keydown)="oKeyDown.emit($event)"
                (keyup)="oKeyUp.emit($event)"
                [disabled]="iDisabled">
            <ng-content></ng-content>
        </button>
    `,
    styleUrls: ['./component.css'],
})

export class ButtonComponent {
    @Input() iType: 'button' | 'submit' = 'button';
    @Input() iColor: 'green' | 'white' | 'red' = 'green';
    @Input() iSize: 'normal' | 'small' = 'normal';
    @Input() iDisabled = false;
    @Output() oClicked = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oKeyUp = new EventEmitter();
}
