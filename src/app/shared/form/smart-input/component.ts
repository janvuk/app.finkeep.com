import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { FormService } from '../form.service';

export declare type SmartInputType = 'text' | 'password' | 'number';
export declare type SmartInputTheme = 'white' | 'black';

export interface SmartInputValue {
    id?: number;
    title: string;
    subtitle?: string;
    inverted?: boolean;
    children?: {
        [key: string]: 'string' | SmartInputValue[];
    };
    meta?: object;
}

@Component({
    selector: 'app-form-smart-input',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})
export class SmartInputComponent implements OnInit {
    formControl: AbstractControl;
    values: SmartInputValue[] = [];
    canDelete = false;
    shiftDown = false;
    focused = false;
    doNotMoveCaret = false;
    ignoreMouseDown = false;
    ignoreInputFocus = false;
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iCanAdd = true;
    @Input() iMultiple = true;
    @Input() iCanInvert = true;
    @Input() bValueText = '';
    @Output() bValueTextChange = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @Output() oKeyUp = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oClickedOutside = new EventEmitter();
    @Output() oPillDoubleClick = new EventEmitter();
    @ViewChild('inputText') inputText;
    @ViewChild('inputHidden') inputHidden;

    @HostListener('document:click', ['$event']) clickedOutside($event) {
        if (!this.eRef.nativeElement.contains($event.target)) {
            this.oClickedOutside.emit();
        }
    }

    @HostListener('window:keydown', ['$event']) windowKeyDown($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        }
    }

    @HostListener('window:keyup', ['$event']) windowKeyUp($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = false;
        }
    }

    constructor(private eRef: ElementRef,
                private formService: FormService) {
    }

    ngOnInit() {
        this.formControl = this.iFormGroup.controls[this.iFormControlName];
        this.values = this.formService.getValuesFromValue(this.formControl.value);
        // On value change
        this.formControl.valueChanges.subscribe(() => {
            this.values = this.formService.getValuesFromValue(this.formControl.value);
        });
    }

    updateValueFromValues() {
        setTimeout(() => {
            this.formControl.setValue(JSON.stringify(this.values));
        }, 0);
    }

    inputFocus() {
        if (!this.ignoreInputFocus) {
            this.inputText.nativeElement.focus();
            if (this.doNotMoveCaret === false) {
                this.caretToEnd();
            } else {
                this.doNotMoveCaret = false;
            }
        }
        this.ignoreInputFocus = false;
    }

    clearValue(index) {
        setTimeout(() => {
            if (this.values.length > 0) {
                this.values.splice(index, 1);
            }
            this.updateValueFromValues();
            this.reFocusIfNeeded();
        }, 0);
    }

    clearAll() {
        setTimeout(() => {
            this.values = [];
            this.updateValueFromValues();
            this.reFocusIfNeeded();
        }, 0);
    }


    toggle(index) {
        if (this.iCanInvert) {
            this.values[index].inverted = !this.values[index].inverted;
            this.updateValueFromValues();
        }
        this.reFocusIfNeeded();
    }

    focus() {
        this.focused = true;
        this.oFocus.emit();
    }

    blur() {
        if (!this.ignoreMouseDown) {
            this.focused = false;
            this.canDelete = false;
            this.oBlur.emit();
        }
        this.ignoreMouseDown = false;
    }

    caretToEnd() {
        const range = document.createRange();
        range.selectNodeContents(this.inputText.nativeElement);
        range.collapse(false);
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);

    }

    valueTextChange() {
        this.bValueText = this.inputText.nativeElement.innerText;
        if (this.values.length > 0) {
            this.canDelete = false;
        }
        this.bValueTextChange.emit(this.bValueText);
    }

    keyDown($event) {
        if ($event.keyCode === 13) {
            // Enter
            $event.preventDefault();
            if (this.iCanAdd) {
                console.log(this.bValueText);
                this.addValue({
                    title: this.bValueText,
                    inverted: this.iCanInvert && this.shiftDown,
                });
                this.bValueText = '';
            }
        } else if ($event.keyCode === 27) {
            // Escape
            this.inputText.nativeElement.blur();
            this.oClickedOutside.emit();
        } else if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        } else if ($event.keyCode === 8) {
            // Backspace
            if (this.values.length > 0 && this.bValueText.length === 0) {
                if (this.canDelete) {
                    this.values.pop();
                    this.updateValueFromValues();
                    this.canDelete = false;
                } else {
                    this.canDelete = true;
                }
            }
        }
        this.oKeyDown.emit($event);
    }

    addValue(value: SmartInputValue) {
        if (value.title.length > 0) {
            let valuesIndex = -1;
            for (let i = 0; i < this.values.length; i++) {
                if ((value.id && this.values[i].id === value.id) || (this.values[i].title === value.title)) {
                    valuesIndex = i;
                    break;
                }
            }
            value.inverted = this.iCanInvert && this.shiftDown;
            if (valuesIndex < 0) {
                if (this.iMultiple) {
                    this.values.push(value);
                } else {
                    this.values = [value];
                }
            } else {
                this.values.splice(valuesIndex, 1);
            }
            this.updateValueFromValues();
        }
    }

    keyUp($event) {
        this.oKeyUp.emit($event);
    }

    reFocusIfNeeded() {
        if (this.focused) {
            setTimeout(() => {
                this.inputText.nativeElement.focus();
            }, 0);
        }
    }
}
