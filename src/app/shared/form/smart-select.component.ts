import { Component, Input, Output, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { SmartInputTheme, SmartInputType, SmartInputValue } from './smart-input/component';
import { SmartCompleteData } from './smart-complete/component';
import { AbstractControl, FormGroup } from '@angular/forms';
import { FormService } from './form.service';

@Component({
    selector: 'app-form-smart-select',
    template: `
        <app-form-smart-input
            #smartInput
            [iFormGroup]="iFormGroup"
            [iFormControlName]="iFormControlName"
            [iLabel]="iLabel"
            [iType]="iType"
            [(bValueText)]="bValueText"
            [iCanAdd]="iCanAdd"
            [iTheme]="iTheme"
            [iCanInvert]="iCanInvert"
            [iMultiple]="iMultiple"
            (oKeyDown)="oKeyDown.emit($event); tabbedOut($event);"
            (oFocus)="this.oFocus.emit(); this.opened = true;"
            (oBlur)="oBlur.emit()"
            (oClickedOutside)="opened = false">
            <app-form-smart-complete
                *ngIf="opened"
                [iData]="iData"
                [iValueText]="bValueText"
                [iValues]="values"
                [iKeyDown]="oKeyDown"
                [iCanManage]="iCanManage"
                (oSelected)="oSmartCompleteSelected($event)"
                (oManageClicked)="manageClicked()"></app-form-smart-complete>
        </app-form-smart-input>
    `,
})

export class SmartSelectComponent implements OnInit {
    formControl: AbstractControl;
    values: SmartInputValue[] = [];
    opened = false;
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iMultiple = true;
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iCanManage = true;
    @Input() iCanInvert = true;
    @Input() iData: SmartCompleteData[] = [];
    @Input() iCanAdd = false;
    @Input() bValueText = '';
    @Output() bValueTextChange = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();
    @ViewChild('smartInput') smartInput;

    constructor(private formService: FormService) {
    }

    ngOnInit() {
        this.formControl = this.iFormGroup.controls[this.iFormControlName];
        this.values = this.formService.getValuesFromValue(this.formControl.value);
        // On value change
        this.formControl.valueChanges.subscribe(() => {
            this.values = this.formService.getValuesFromValue(this.formControl.value);
        });
    }

    oSmartCompleteSelected(value) {
        setTimeout(() => {
            this.smartInput.addValue(value);
            this.smartInput.inputText.nativeElement.focus();
            this.bValueText = '';
        }, 0);
    }

    tabbedOut($event) {
        if ($event.keyCode === 9) {
            this.opened = false;
        }
    }

    manageClicked() {
        console.log('manage');
    }
}
