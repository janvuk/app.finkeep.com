import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DateService } from '../date.service';
import * as moment from 'moment';

@Component({
    selector: 'app-form-smart-calendar',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})

export class SmartCalendarComponent implements OnInit {
    view: 'days' | 'months' | 'years' = 'days';
    value: string;
    currentYear: number;
    currentMonth: number;
    currentMonthTitle: string;
    yearsArray = [];
    monthsArray = [];
    daysOfWeekArray = [];
    daysArray = [];
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';

    constructor(private dateService: DateService) {
        this.monthsArray = dateService.monthsArray;
        this.daysOfWeekArray = dateService.daysOfWeekArray;
        this.yearsArray = dateService.yearsArray;
    }

    ngOnInit() {
        // Listen to form control value changes
        this.iFormGroup.controls[this.iFormControlName].valueChanges.subscribe(() => {
            setTimeout(() => {
                this.valueChanged(false);
            }, 0);
        });
        // Force value change
        this.valueChanged(true);
    }

    /**
     * On value change
     */
    valueChanged(forceSet) {
        const momentValue = moment(this.iFormGroup.controls[this.iFormControlName].value, this.dateService.formats);
        if (momentValue.isValid() && momentValue.year() >= 1900) {
            this.value = momentValue.format(this.dateService.format);
            this.currentYear = parseInt(momentValue.format('YYYY'), 10);
            this.setMonthCurrent(parseInt(momentValue.format('M'), 10));
            this.getDaysArray();
        } else if (forceSet) {
            this.currentYear = parseInt(moment().format('YYYY'), 10);
            this.setMonthCurrent(parseInt(moment().format('M'), 10));
            this.getDaysArray();
        }
    }

    getDaysArray() {
        this.daysArray = [];
        const date = moment(`1-${this.currentMonth}-${this.currentYear}`, this.dateService.formats);
        const startOfMonth = date.clone().startOf('month');
        const weeksInMonth = this.getWeeksInMonth(date);
        for (let i = 0; i < weeksInMonth; i++) {
            const momentWeek = startOfMonth.clone().add(i, 'weeks').startOf('isoWeek');
            this.daysArray.push(
                Array(7).fill(0).map((n, j) => {
                    const day = momentWeek.clone().add(n + j, 'day');
                    return {
                        date: day,
                        formatted: day.format(this.dateService.format).toString(),
                        day: day.format('DD').toString(),
                        month: parseInt(day.format('M'), 10),
                    };
                }),
            );
        }
    }

    getWeeksInMonth(date) {
        let weekFirst = date.clone().startOf('month').isoWeek();
        let weekLast = date.clone().endOf('month').isoWeek();
        if (weekFirst > weekLast) {
            if (weekLast === 1) {
                weekLast = date.clone().endOf('month').subtract(1, 'week').isoWeek() + 1;
            } else {
                weekFirst = 0;
            }
        }
        return weekLast - weekFirst + 1;
    }

    changeMonth(isNext) {
        if (isNext) {
            this.currentMonth++;
            if (this.currentMonth > 12) {
                this.currentMonth = 1;
                this.currentYear++;
            }
        } else {
            this.currentMonth--;
            if (this.currentMonth < 1) {
                this.currentMonth = 12;
                this.currentYear--;
            }
        }
        this.setMonthCurrent(this.currentMonth);
    }

    setMonthCurrent(currentMonth) {
        this.currentMonth = currentMonth;
        this.currentMonthTitle = this.dateService.getMonthTitleFromIndex(this.currentMonth);
        this.getDaysArray();
        this.changeView('days');
    }

    changeYear(isNext) {
        if (isNext) {
            this.currentYear++;
        } else {
            this.currentYear--;
        }
        this.setYearCurrent(this.currentYear);
    }

    setYearCurrent(currentYear) {
        this.currentYear = currentYear;
        this.getDaysArray();
        this.changeView('days');
    }

    changeView(view) {
        setTimeout(() => {
            this.view = view;
        }, 0);
    }

    setValue(date) {
        this.iFormGroup.controls[this.iFormControlName].setValue(date.format(this.dateService.format).toString());
    }
}
