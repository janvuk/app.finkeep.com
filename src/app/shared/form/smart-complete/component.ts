import { Component, Input, Output, EventEmitter, OnChanges, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SmartInputValue } from '../smart-input/component';
import { ApiService } from '../../api.service';

declare const OverlayScrollbars: any;

export interface SmartCompleteData {
    id: number;
    title: string;
    parentId: number;
    children?: [{}];
}

@Component({
    selector: 'app-form-smart-complete',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})

export class SmartCompleteComponent implements OnInit, OnChanges, AfterViewInit {
    itemsSorted: SmartCompleteData[] = [];
    itemsFiltered = [];
    itemIndexX = -1;
    itemIndexY = -1;
    scrollbar;
    scrollbarViewport;
    scrollStep = 29;
    scrollSpeed = 150;
    @Input() iData: SmartCompleteData[] = [];
    @Input() iCanManage = true;
    @Input() iValueText = '';
    @Input() iValues: SmartInputValue[] = [];
    @Input() iKeyDown: EventEmitter<any>;
    @Input() iScrollItemsMax = 5;
    @Output() oSelected: EventEmitter<{}> = new EventEmitter();
    @Output() oManageClicked: EventEmitter<{}> = new EventEmitter();
    @ViewChild('elScroll') elScroll;

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        /**
         * Key down
         */
        this.iKeyDown.subscribe(($event) => {
            switch ($event.keyCode) {
                // Up
                case 38:
                    $event.preventDefault();
                    this.keyDownMove(false, false);
                    break;
                // Down
                case 40:
                    $event.preventDefault();
                    this.keyDownMove(false, true);
                    break;
                // Left
                case 37:
                    if (this.itemIndexY >= 0) {
                        $event.preventDefault();
                    }
                    this.keyDownMove(true, false);
                    break;
                // Right
                case 39:
                    if (this.itemIndexY >= 0) {
                        $event.preventDefault();
                    }
                    this.keyDownMove(true, true);
                    break;
                // Enter
                case 13:
                    $event.preventDefault();
                    if (this.itemIndexY >= 0) {
                        if (this.itemsFiltered[this.itemIndexY][this.itemIndexX].arrow) {
                            this.arrowToggle(this.itemIndexX, this.itemIndexY);
                        } else {
                            this.selectItem(this.itemIndexX, this.itemIndexY);
                            this.itemIndexY = -1;
                        }
                    }
                    break;
            }
        });
    }

    ngOnChanges(changes) {
        this.itemIndexY = -1;
        this.itemsSorted = this.apiService.rowsToNested(this.iData);
        this.itemsFiltered = this.filter(this.itemsSorted);
    }

    ngAfterViewInit() {
        this.scrollbar = OverlayScrollbars(this.elScroll.nativeElement, {});
        this.scrollbarViewport = this.elScroll.nativeElement.querySelector('.os-viewport');
    }

    matcher(string, search) {
        return string.toLowerCase().indexOf(search.toLowerCase()) >= 0;
    }

    filter(items) {
        const response = [];
        items.forEach((item) => {
            if (this.matcher(item.title, this.iValueText)) {
                response.push(this.itemWithArrow(item));
            }
            if (item.children.length > 0 && this.iValueText.length > 0) {
                const children = this.filter(item.children);
                children.forEach((child) => {
                    response.push(this.itemWithArrow(item).concat(child));
                });
            }
        });
        return response;
    }

    itemWithArrow(item, lvl = 0) {
        if (item.children.length > 0) {
            return [Object.assign(item, {
                arrow: false,
                lvl: lvl,
            }), {
                arrow: true,
                lvl: lvl,
                opened: false,
                children: item.children,
            }];
        }
        return [Object.assign(item, {
            arrow: false,
            lvl: lvl,
        })];
    }

    arrowToggle(x, y) {
        if (this.itemsFiltered[y][x].opened) {
            this.arrowClose(y);
        } else {
            this.arrowOpen(x, y);
        }
    }

    arrowClose(y) {
        let childrenOpenedLength = 0;
        for (let i = (y + 1); i < this.itemsFiltered.length; i++) {
            if (this.itemsFiltered[i][0].lvl <= this.itemsFiltered[y][0].lvl) {
                break;
            }
            childrenOpenedLength++;
        }
        this.itemsFiltered.splice(y + 1, childrenOpenedLength);
        for (let x = 0; x < this.itemsFiltered[y].length; x++) {
            this.itemsFiltered[y][x].opened = false;
        }
    }

    arrowOpen(x, y) {
        this.arrowClose(y);
        this.itemsFiltered[y][x].children.forEach((item, i) => {
            this.itemsFiltered.splice(y + 1 + i, 0, this.itemWithArrow(item, this.itemsFiltered[y][x].lvl + 1));
        });
        this.itemsFiltered[y][x].opened = true;
    }

    selectItem(x, y) {
        const item = this.itemsFiltered[y][x];
        this.oSelected.emit({
            id: item.id,
            title: item.title,
            inverted: false,
        });
    }

    keyDownMove(isXAxis, isIncrement) {
        const axisKey = isXAxis ? 'itemIndexX' : 'itemIndexY';
        this[axisKey] = isIncrement ? this[axisKey] += 1 : this[axisKey] -= 1;
        const itemY = this.itemsFiltered[this.itemIndexY];
        if (isXAxis && this.itemIndexY >= 0 && itemY) {
            if (this.itemIndexX < 0) {
                this.itemIndexX = itemY.length - 1;
            } else if (this.itemIndexX >= itemY.length) {
                this.itemIndexX = 0;
            }
        } else {
            if (this.itemIndexY < -1) {
                this.itemIndexY = this.itemsFiltered.length - 1;
            } else if (this.itemIndexY >= this.itemsFiltered.length) {
                this.itemIndexY = 0;
            }
            if (this.itemIndexY >= 0 && itemY) {
                this.itemIndexX = itemY.length - 1;
                for (let x = 0; x < itemY.length; x++) {
                    if (itemY[x].arrow && itemY[x].opened) {
                        this.itemIndexX = x;
                        break;
                    }
                }
                if (itemY[this.itemIndexX].arrow && !itemY[this.itemIndexX].opened) {
                    --this.itemIndexX;
                }
            }
            // Scroll
            this.scrollbar.scrollStop();
            const scrollTop = this.scrollbarViewport.scrollTop;
            const scrollViewportIndexMin = Math.floor(scrollTop / this.scrollStep);
            const scrollViewportIndexMax = scrollViewportIndexMin + this.iScrollItemsMax - 1;
            let scrollTo = null;
            if (this.itemIndexY <= scrollViewportIndexMin) {
                scrollTo = this.itemIndexY * this.scrollStep;
            } else if (this.itemIndexY > scrollViewportIndexMax) {
                scrollTo = (this.itemIndexY - this.iScrollItemsMax + 1) * this.scrollStep;
            }
            this.scrollbar.scroll({y: scrollTo}, this.scrollSpeed);
        }
    }

    isUsed(id) {
        for (const item of this.iValues) {
            if (item.id === id) {
                return true;
            }
        }
        return false;
    }
}
