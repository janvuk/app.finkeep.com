import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class DateService {
    yearsArray = [];
    monthsArray = [];
    daysOfWeekArray = [];
    yearsAhead = 10;
    format = 'DD.MM.YYYY';
    formats = ['D.M.YYYY', 'D.MM.YYYY', 'DD.M.YYYY', this.format];

    constructor() {
        /**
         * Populate months array
         */
        let monthsRow = [];
        for (let i = 1; i <= 12; i++) {
            monthsRow.push(this.getMonthTitleFromIndex(i));
            if (i % 4 === 0) {
                this.monthsArray.push(monthsRow);
                monthsRow = [];
            }
        }
        /**
         * Populate days of week array
         */
        for (let i = 1; i <= 7; i++) {
            this.daysOfWeekArray.push(moment().day(i).format('dd'));
        }
        /**
         * Populate years array. Has to be after "currentYear" is set
         */
        const currentYear = parseInt(moment().format('YYYY'), 10);
        let yearsRow = [];
        for (let i = 1; i <= 49; i++) {
            yearsRow.push((currentYear + this.yearsAhead - 42 + i).toString());
            if (i % 7 === 0) {
                this.yearsArray.push(yearsRow);
                yearsRow = [];
            }
        }
    }

    getMonthTitleFromIndex(monthIndex) {
        return moment().day(1).month(monthIndex - 1).format('MMM');
    }
}
