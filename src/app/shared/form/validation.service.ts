import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ng2-cookies';
import { FormControl, FormGroup } from '@angular/forms';
import { FormService } from './form.service';

@Injectable()
export class ValidationService {
    errorMessages = {
        'minPills': 'Minimum %d must be selected.',
        'fromBiggerThanTo': 'Form cannot be bigger than to.',
    };

    constructor(private http: HttpClient,
                private cookieService: CookieService,
                private formService: FormService) {
    }

    parseJson(value) {
        try {
            return JSON.parse(value);
        } catch (e) {
            return [];
        }
    }

    getErrorMessage(c: FormControl) {
        return '';
    }

    getErrorMessages(g: FormGroup) {
        return '';
    }

    minPills(min: number) {
        return (c: FormControl) => {
            if (this.formService.getValuesFromValue(c.value).length >= min) {
                return null;
            }
            return {errorMessages: min};
        };
    }

    amountRangeGroup(g: FormGroup) {
        const errors = {};
        // Check if from is bigger than to
        const fromValue = g.controls.from.value;
        const toValue = g.controls.from.value;
        if (this.formService.isNotEmpty(fromValue) &&
            this.formService.isNotEmpty(toValue) &&
            fromValue > toValue) {
            errors['fromBiggerThanTo'] = false;
        }
        // todo: If only currency selected it cannot be grouped currency
        if (Object.keys(errors).length > 0) {
            return errors;
        }
        return null;
    }
}
