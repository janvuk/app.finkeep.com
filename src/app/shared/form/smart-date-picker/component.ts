import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { SmartInputTheme, SmartInputType } from '../smart-input/component';
import { FormGroup } from '@angular/forms';
import { SmartCompleteData } from '../smart-complete/component';

@Component({
    selector: 'app-form-smart-date-picker',
    templateUrl: './component.html',
    styleUrls: ['./component.css'],
})

export class SmartDatePickerComponent {
    opened = false;
    shiftDown = false;
    @Input() iFormGroup: FormGroup;
    @Input() iFormControlName = '';
    @Input() iLabel = '';
    @Input() iValue = '';
    @Input() iMultiple = true;
    @Input() iCanAdd = false;
    @Input() iType: SmartInputType = 'text';
    @Input() iTheme: SmartInputTheme = 'black';
    @Input() iData: SmartCompleteData[] = [];
    @Input() iToRight = false;
    @Input() bValueText = '';
    @Output() bValueTextChange = new EventEmitter();
    @Output() oKeyDown = new EventEmitter();
    @Output() oFocus = new EventEmitter();
    @Output() oBlur = new EventEmitter();

    @HostListener('window:keydown', ['$event']) windowKeyDown($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = true;
        }
    }

    @HostListener('window:keyup', ['$event']) windowKeyUp($event) {
        if ($event.keyCode === 16) {
            // Shift
            this.shiftDown = false;
        }
    }

    tabCloses($event, shiftDown) {
        if ($event.keyCode === 9 && (!shiftDown && !this.shiftDown) || (shiftDown && this.shiftDown)) {
            this.opened = false;
        }
    }
}
