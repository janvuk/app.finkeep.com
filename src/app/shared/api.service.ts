import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ng2-cookies';
import { SmartCompleteData } from './form/smart-complete/component';

@Injectable()
export class ApiService {

    constructor(private http: HttpClient,
                private cookieService: CookieService) {
    }

    request(data) {
        data = Object.assign({
            method: 'get',
            url: '',
            params: {},
            upload: false,
            sessionHash: false,
        }, data);
        const url = environment.apiBase + data.url;
        let body;
        /**
         * Headers
         */
        const headers = {};
        if (data.method === 'post' && data.upload === false) {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
        /**
         * Params
         */
        if (data.method === 'post' || data.method === 'put') {
            if (data.upload === false) {
                body = new HttpParams();
                if (data.sessionHash === true) {
                    body = body.set('session_hash', this.cookieService.get(environment.userHashCookieName));
                }
                for (const key in data.params) {
                    if (data.params.hasOwnProperty(key)) {
                        body = body.set(key, data.params[key]);
                    }
                }
            } else {
                body = new FormData();
                if (data.sessionHash === true) {
                    body.append('session_hash', this.cookieService.get(environment.userHashCookieName));
                }
                for (const key in data.params) {
                    if (data.params.hasOwnProperty(key)) {
                        body.append(key, data.params[key]);
                    }
                }
            }
        } else if (data.method === 'get' || data.method === 'delete') {
            if (data.sessionHash === true) {
                data.params['session_hash'] = this.cookieService.get(environment.userHashCookieName);
            }
        }
        /**
         * Request
         */
        const requestOptions = {
            headers: headers,
        };
        if (data.headers === true) {
            requestOptions['observe'] = 'response';
        }
        if (data.method === 'post') {
            return this.http.post(url, body.toString(), requestOptions);
        } else if (data.method === 'put') {
            return this.http.put(url, body, requestOptions);
        } else if (data.method === 'delete') {
            requestOptions['params'] = data.params;
            return this.http.delete(url, requestOptions);
        } else if (data.method === 'get') {
            requestOptions['params'] = data.params;
            return this.http.get(url, requestOptions);
        }
    }

    rowsToNested(rows, parentId = null) {
        const response = [];
        for (const i in rows) {
            if (rows[i].parentId === parentId) {
                response.push(Object.assign({
                    children: this.rowsToNested(rows, rows[i].id),
                }, rows[i]));
            }
        }
        return response;
    }

    getAccounts(): SmartCompleteData[] {
        return [
            {
                id: 1,
                title: 'CMDigitalt',
                parentId: null,
            },
            {
                id: 2,
                title: 'CRYSTALmediat',
                parentId: null,
            },
            {
                id: 3,
                title: 'Personalt',
                parentId: null,
            },
            {
                id: 4,
                title: 'Paypalt',
                parentId: 3,
            },
            {
                id: 5,
                title: 'Skrillt',
                parentId: 3,
            },
            {
                id: 6,
                title: 'Intersat',
                parentId: 1,
            },
            {
                id: 7,
                title: 'Privatet',
                parentId: 6,
            },
            {
                id: 8,
                title: 'Privatet',
                parentId: 6,
            },
        ];
    }

    getCurrencies(): SmartCompleteData[] {
        return [
            {
                id: 1,
                title: 'EUR',
                parentId: null,
            },
            {
                id: 2,
                title: 'USD',
                parentId: null,
            },
            {
                id: 3,
                title: 'RSD',
                parentId: null,
            },
        ];
    }

    getTransactionTypes(): SmartCompleteData[] {
        return [
            {
                id: 1,
                title: 'Income',
                parentId: null,
            },
            {
                id: 2,
                title: 'Expence',
                parentId: null,
            },
            {
                id: 3,
                title: 'Transfer',
                parentId: null,
            },
        ];
    }

    getCategories(): SmartCompleteData[] {
        return [
            {
                id: 1,
                title: 'Services',
                parentId: null,
            },
            {
                id: 2,
                title: 'Development',
                parentId: 1,
            },
            {
                id: 3,
                title: 'PHP',
                parentId: 2,
            },
            {
                id: 4,
                title: 'Hosting',
                parentId: 1,
            },
            {
                id: 5,
                title: 'Design',
                parentId: 1,
            },
            {
                id: 6,
                title: 'Misc',
                parentId: null,
            },
            {
                id: 7,
                title: 'NodeJS',
                parentId: 2,
            },
            {
                id: 8,
                title: 'Express',
                parentId: 7,
            },
        ];
    }
}
