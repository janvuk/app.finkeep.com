import { AfterViewInit, Component } from '@angular/core';

declare const OverlayScrollbars: any;

@Component({
    selector: 'app-root',
    template: `
        <router-outlet></router-outlet>
    `,
})
export class AppComponent implements AfterViewInit {
    ngAfterViewInit() {
        OverlayScrollbars(document.querySelectorAll('body'), {});
    }
}
